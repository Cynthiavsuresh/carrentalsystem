package com.example.carrental.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.carrental.dto.RentalStatus;
import com.example.carrental.dto.ResponseDto;
import com.example.carrental.service.RentalService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequiredArgsConstructor
public class RentalController {

	private final RentalService rentalService;
	
	/**
	 * This method is used to approve or reject the Rental requests based on
	 * @param rentalId
	 * @return response message as successfully approved or rejected
	 */
	@PatchMapping("/rental/{rentalId}")
	public ResponseEntity<ResponseDto> UpdateStatus(@PathVariable Long rentalId, @RequestParam RentalStatus rentalStatus,
			@RequestParam Long userId ,@RequestParam String reason){
		log.info("In this method Iam updating the status of Rental requests");
		ResponseDto response = rentalService.UpdateStatus(rentalId,rentalStatus,userId,reason);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
