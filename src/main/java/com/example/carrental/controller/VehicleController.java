package com.example.carrental.controller;



import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.carrental.dto.ViewRentalResponse;
import com.example.carrental.dto.ViewVehicleResponse;
import com.example.carrental.service.VehicleService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/vehicles")
@RequiredArgsConstructor
public class VehicleController {
	
	private final VehicleService vehicleservice;
	
	@GetMapping
	public ResponseEntity<ViewVehicleResponse> viewVehicle( @PathVariable Long userId,
				@RequestParam(required = false, defaultValue = "0") Integer page,
				@RequestParam(required = false, defaultValue = "2") Integer size) 
	{
			log.info("RentalController - toViewListOfrental details");
			ViewVehicleResponse vresponse = vehicleservice.viewVehicle(userId,page, size);
			return new ResponseEntity<>(vresponse, HttpStatus.OK);
	}

}
