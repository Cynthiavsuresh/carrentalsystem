package com.example.carrental.service;

import com.example.carrental.dto.ApiResponse;
import com.example.carrental.dto.ViewVehicleResponse;

public interface VehicleService {

	ApiResponse viewVehicle(Integer page, Integer size);

	ViewVehicleResponse viewVehicle(Long userId, Integer page, Integer size);

}
