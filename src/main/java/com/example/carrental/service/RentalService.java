package com.example.carrental.service;

import com.example.carrental.dto.RentalStatus;
import com.example.carrental.dto.ResponseDto;

public interface RentalService {


	ResponseDto UpdateStatus(Long rentalId, RentalStatus rentalStatus, Long userId, String reason);

}
