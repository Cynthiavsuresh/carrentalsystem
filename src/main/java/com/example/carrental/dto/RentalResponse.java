package com.example.carrental.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RentalResponse {
	private Long userId;
	private Long vehicleId;
	private LocalDate startDate;
	private LocalDate endDate;
	private RentalStatus rentalStatus;
	private String reason;

}
