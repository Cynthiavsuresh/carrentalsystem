package com.example.carrental.dto;

public record ViewVehicleResponse(ApiResponse apiResponse, VehicleResponse vehicleResponse) {

}
