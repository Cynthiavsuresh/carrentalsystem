package com.example.carrental.dto;

import java.math.BigDecimal;

public record VehicleResponse(Long vehicle_Id, String brand,Type type,Status status, BigDecimal price_per_day, String reg_no, String model) {

}
