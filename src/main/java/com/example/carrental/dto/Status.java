package com.example.carrental.dto;

public enum Status {
	AVAILABLE, UNAVAILABLE, BOOKED

}
