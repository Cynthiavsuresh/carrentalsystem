package com.example.carrental.dto;

public enum RentalStatus {
	ACCEPT, REJECT
}
