package com.example.carrental.dto;

import org.springframework.data.domain.Page;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViewRentalResponse {
	private ApiResponse apiResponse;
	private Page<RentalResponse> rentalResponse;
	
	

}
