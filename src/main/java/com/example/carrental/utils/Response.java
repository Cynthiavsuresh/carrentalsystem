package com.example.carrental.utils;

public interface Response {
	// Success Codes
	Long SUCCESS_STATUS_CODE = 2000l;

	// Success Messages
	String SUCCESS_STATUS_MESSAGE = "Successfully executed";

	// Error Codes
	Long METHOD_ARGUMENT_EXCEPTION_CODE = 0000l;
	Long EMPTY_STATUS_CODE = 4004l;

	// Error Messages
	String METHOD_ARGUMENT_EXCEPTION_MESSAGE = "Enter valid data, The values must be present";
	String HTTP_MESSAGE_EXCEPTION_MESSAGE = "Provide valid details, pass proper Json format";
	String HANDLER_METHOD_EXCEPTION_INPUT_MESSAGE = "Invalid input";
	String HANDLER_METHOD_EXCEPTION_MESSAGE = "Please enter valid input";

	String USER_NOT_FOUND_MESSAGE = "user cannot be found";
	Long USER_NOT_FOUND_CODE = 1000l;
	// BUSINESS EXCEPTION CODES
	Long NO_RECORD_FOUND_CODE = 1001l;
	Long INVALID_TRAINEE_ROLE_CODE = 1002l;
	Long INVALID_COURSE_ID_CODE = 1003l;
	Long ALREADY_ENROLLED_CODE = 1004l;
	Long ENROLLMENT_CONFLICT_CODE = 1005l;
	Long COURSE_ALREADY_ENDED_CODE = 1006l;
	Long COURSE_LIMIT_EXCEED_CODE = 1007l;
	Long COURSE_OVERLAP_CODE = 1008l;
	Long COURSE_CANCEL_VOILATION_CODE = 1009l;

	// BUSINESS EXCEPTION CODES
	String NO_RECORD_FOUND_MESSAGE = "No records found";
	String INVALID_TRAINEE_ROLE_MESSAGE = "Invalid trainee id";
	String INVALID_COURSE_ID_MESSAGE = "Invalid course id";
	String ALREADY_ENROLLED_MESSAGE = "Course already enrolled";
	String ENROLLMENT_CONFLICT_MESSAGE = "Enrollment date conflict";
	String COURSE_ALREADY_ENDED_MESSAGE = "Course already ended";
	String COURSE_LIMIT_EXCEED_MESSAGE = "Course Limit exceeded";
	String COURSE_OVERLAP_MESSAGE = "Course overlap";
	String COURSE_CANCEL_VOILATION_MESSAGE = "Course cannot be cancelled ";
}
