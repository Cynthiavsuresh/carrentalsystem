package com.example.carrental.exception;

public class InvalidRoleException extends GlobalException {



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InvalidRoleException(Long invalidTraineeRoleCode, String invalidTraineeRoleMessage) {
		super(invalidTraineeRoleMessage);

		// TODO Auto-generated constructor stub
	}

	public InvalidRoleException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidRoleException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
