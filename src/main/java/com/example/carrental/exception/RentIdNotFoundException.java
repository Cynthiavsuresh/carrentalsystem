package com.example.carrental.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RentIdNotFoundException extends GlobalException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RentIdNotFoundException(String code,String message) {
		super(code,message);
	}

}
