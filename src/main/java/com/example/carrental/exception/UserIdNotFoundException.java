package com.example.carrental.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserIdNotFoundException extends GlobalException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserIdNotFoundException(String code,String message) {
		super(code,message);
	}

}
