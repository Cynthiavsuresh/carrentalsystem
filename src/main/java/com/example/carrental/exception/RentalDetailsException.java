package com.example.carrental.exception;

public class RentalDetailsException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RentalDetailsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RentalDetailsException(Long noRecordFoundCode, String noRecordFoundMessage) {
		super(noRecordFoundCode,noRecordFoundMessage);
		// TODO Auto-generated constructor stub
	}

	
	

}
