package com.example.carrental.serviceImpl;

import org.springframework.stereotype.Service;

import com.example.carrental.dto.RentalStatus;
import com.example.carrental.dto.ResponseDto;
import com.example.carrental.dto.Role;
import com.example.carrental.dto.Status;
import com.example.carrental.entity.Rental;
import com.example.carrental.entity.User;
import com.example.carrental.entity.Vehicle;
import com.example.carrental.exception.RentIdNotFoundException;
import com.example.carrental.exception.UserIdNotFoundException;
import com.example.carrental.repository.RentalRepository;
import com.example.carrental.repository.UserRepository;
import com.example.carrental.repository.VehicleRepository;
import com.example.carrental.service.RentalService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class RentalServiceImpl implements RentalService {

	private final RentalRepository rentalRepository;
	private final UserRepository userRepository;
	private final VehicleRepository vehicleRepository;

	/**
	 * to update the status of the rental car request
	 */
	@Override
	public ResponseDto UpdateStatus(Long rentalId, RentalStatus rentalStatus, Long userId, String reason) {
		log.info("RentalServiceImpl - updateStatus");
		Rental rental = rentalRepository.findById(rentalId).orElseThrow(() -> {
			log.error("Rent id not found");
			throw new RentIdNotFoundException("4001","RentalId not found");
		});

		User user = userRepository.findById(userId).orElseThrow(() -> {
			log.error("user id not found");
			throw new UserIdNotFoundException("4001","UserId not found");
		});

		Vehicle vehicle = rental.getVehicleId();

		if (!user.getRole().equals(Role.AGENT)) {
			log.warn("Role mismatched");
			throw new UserIdNotFoundException("4002	","Role mismatched");
		}

		rental.setRentalstatus(rentalStatus);
		rental.setReason(reason);
		rentalRepository.save(rental);
		if (rentalStatus.equals(RentalStatus.ACCEPT)) {
			vehicle.setStatus(Status.BOOKED);
			vehicleRepository.save(vehicle);
		}

		ResponseDto response = new ResponseDto();
		response.setReason(reason);
		response.setStatusCode("2001");
		log.info("Rental request Accepted Successfully");
		response.setStatusMessage("Rental request processed");
		return response;
	}

}
