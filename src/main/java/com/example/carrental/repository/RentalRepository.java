package com.example.carrental.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.carrental.entity.Rental;

public interface RentalRepository extends JpaRepository<Rental, Long> {

}
