package com.example.carrental.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.carrental.entity.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

}
