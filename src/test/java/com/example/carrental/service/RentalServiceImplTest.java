package com.example.carrental.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.carrental.dto.RentalStatus;
import com.example.carrental.dto.ResponseDto;
import com.example.carrental.dto.Role;
import com.example.carrental.dto.Status;
import com.example.carrental.dto.Type;
import com.example.carrental.entity.Rental;
import com.example.carrental.entity.User;
import com.example.carrental.entity.Vehicle;
import com.example.carrental.exception.RentIdNotFoundException;
import com.example.carrental.exception.UserIdNotFoundException;
import com.example.carrental.repository.RentalRepository;
import com.example.carrental.repository.UserRepository;
import com.example.carrental.repository.VehicleRepository;
import com.example.carrental.serviceImpl.RentalServiceImpl;

@SpringBootTest
public class RentalServiceImplTest {

	@InjectMocks
	RentalServiceImpl rentalServiceImpl;

	@Mock
	RentalRepository rentalRepository;
	@Mock
	UserRepository userRepository;
	@Mock
	VehicleRepository vehicleRepository;

	@Test
	void testUpdateStatus() {
		User user = new User();
		user.setUserid(1l);
		user.setName("John");
		user.setRole(Role.AGENT);
		user.setMobilenos("9876543210");

		Vehicle vehicle = new Vehicle();
		vehicle.setBrand("Honda");
		vehicle.setModel("Civic");
		vehicle.setPrice_per_day(new BigDecimal("500.67"));
		vehicle.setRegno("12345");
		vehicle.setStatus(Status.AVAILABLE);
		vehicle.setType(Type.SEDAN);
		vehicle.setVehicleId(1l);

		Rental rental = new Rental();
		rental.setRentalId(1l);
		rental.setStartdate(LocalDate.now());
		rental.setEnddate(LocalDate.now());
		rental.setReason("Accepted");
		rental.setRentalstatus(RentalStatus.ACCEPT);
		rental.setTotalcost(new BigDecimal("200.98"));
		rental.setUserId(user);
		rental.setVehicleId(vehicle);

		when(rentalRepository.findById(rental.getRentalId())).thenReturn(Optional.of(rental));
		when(userRepository.findById(user.getUserid())).thenReturn(Optional.of(user));

		ResponseDto result = rentalServiceImpl.UpdateStatus(1l, RentalStatus.ACCEPT, 1l, "Accepted");
		assertEquals("Rental request processed", result.getStatusMessage());

	}

	@Test
	void testUpdateStatusRentalInvalid() {
		User user = new User();
		user.setUserid(1l);
		user.setName("John");
		user.setRole(Role.AGENT);
		user.setMobilenos("9876543210");

		Vehicle vehicle = new Vehicle();
		vehicle.setBrand("Honda");
		vehicle.setModel("Civic");
		vehicle.setPrice_per_day(new BigDecimal("500.67"));
		vehicle.setRegno("12345");
		vehicle.setStatus(Status.AVAILABLE);
		vehicle.setType(Type.SEDAN);
		vehicle.setVehicleId(1l);

		Rental rental = new Rental();
		rental.setRentalId(1l);
		rental.setStartdate(LocalDate.now());
		rental.setEnddate(LocalDate.now());
		rental.setReason("Accepted");
		rental.setRentalstatus(RentalStatus.ACCEPT);
		rental.setTotalcost(new BigDecimal("200.98"));
		rental.setUserId(user);
		rental.setVehicleId(vehicle);

		when(rentalRepository.findById(rental.getRentalId())).thenReturn(Optional.empty());
		when(userRepository.findById(user.getUserid())).thenReturn(Optional.of(user));

		RentIdNotFoundException exception = assertThrows(RentIdNotFoundException.class,
				() -> rentalServiceImpl.UpdateStatus(1l, RentalStatus.ACCEPT, 1l, "Accepted"));
		assertEquals("RentalId not found", exception.getMessage());

	}

	@Test
	void testUpdateStatusUserInvalid() {
		User user = new User();
		user.setUserid(1l);
		user.setName("John");
		user.setRole(Role.AGENT);
		user.setMobilenos("9876543210");

		Vehicle vehicle = new Vehicle();
		vehicle.setBrand("Honda");
		vehicle.setModel("Civic");
		vehicle.setPrice_per_day(new BigDecimal("500.67"));
		vehicle.setRegno("12345");
		vehicle.setStatus(Status.AVAILABLE);
		vehicle.setType(Type.SEDAN);
		vehicle.setVehicleId(1l);

		Rental rental = new Rental();
		rental.setRentalId(1l);
		rental.setStartdate(LocalDate.now());
		rental.setEnddate(LocalDate.now());
		rental.setReason("Accepted");
		rental.setRentalstatus(RentalStatus.ACCEPT);
		rental.setTotalcost(new BigDecimal("200.98"));
		rental.setUserId(user);
		rental.setVehicleId(vehicle);

		when(rentalRepository.findById(rental.getRentalId())).thenReturn(Optional.of(rental));
		when(userRepository.findById(user.getUserid())).thenReturn(Optional.empty());

		UserIdNotFoundException exception = assertThrows(UserIdNotFoundException.class,
				() -> rentalServiceImpl.UpdateStatus(1l, RentalStatus.ACCEPT, 1l, "Accepted"));
		assertEquals("UserId not found", exception.getMessage());

	}
	
	@Test
	void testUpdateStatusRolemismatch() {
		User user = new User();
		user.setUserid(1l);
		user.setName("John");
		user.setRole(Role.CUSTOMER);
		user.setMobilenos("9876543210");

		Vehicle vehicle = new Vehicle();
		vehicle.setBrand("Honda");
		vehicle.setModel("Civic");
		vehicle.setPrice_per_day(new BigDecimal("500.67"));
		vehicle.setRegno("12345");
		vehicle.setStatus(Status.AVAILABLE);
		vehicle.setType(Type.SEDAN);
		vehicle.setVehicleId(1l);

		Rental rental = new Rental();
		rental.setRentalId(1l);
		rental.setStartdate(LocalDate.now());
		rental.setEnddate(LocalDate.now());
		rental.setReason("Accepted");
		rental.setRentalstatus(RentalStatus.ACCEPT);
		rental.setTotalcost(new BigDecimal("200.98"));
		rental.setUserId(user);
		rental.setVehicleId(vehicle);

		when(rentalRepository.findById(rental.getRentalId())).thenReturn(Optional.of(rental));
		when(userRepository.findById(user.getUserid())).thenReturn(Optional.of(user));

		UserIdNotFoundException exception = assertThrows(UserIdNotFoundException.class,
				() -> rentalServiceImpl.UpdateStatus(1l, RentalStatus.ACCEPT, 1l, "Accepted"));
		assertEquals("Role mismatched", exception.getMessage());

	}

}
