package com.example.carrental.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.carrental.dto.ApiResponse;
import com.example.carrental.dto.RentalResponse;
import com.example.carrental.dto.RentalStatus;
import com.example.carrental.dto.ViewRentalResponse;
import com.example.carrental.entity.User;
import com.example.carrental.service.RentalService;
import com.example.carrental.utils.Response;

@SpringBootTest
public class RentalControllerTest {
	
	@Mock
	RentalService rentalService;
	
	@InjectMocks
	RentalController rentalController;
	
	ApiResponse apiResponse = new ApiResponse(Response.SUCCESS_STATUS_CODE, Response.SUCCESS_STATUS_MESSAGE);
	RentalResponse RentalResponse = new RentalResponse(1l, 5l, LocalDate.now(), LocalDate.now(), RentalStatus.ACCEPT,
			"Marriage");

	List<RentalResponse> responseList = new ArrayList<>();
	

	@Test
	void testviewCoursessuccess() {
		responseList.add(RentalResponse);
		Page<RentalResponse> pageResponse = new PageImpl<>(responseList);
		ViewRentalResponse response = new ViewRentalResponse(apiResponse, pageResponse);
		
		when(rentalService.viewRentals(anyLong(), anyInt(), anyInt())).thenReturn(response);

		ResponseEntity<ViewRentalResponse> responseEntity = rentalController.viewRentals(1l,0, 2);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(response, responseEntity.getBody());
	}

}
