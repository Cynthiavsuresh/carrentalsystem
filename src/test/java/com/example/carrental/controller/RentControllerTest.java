package com.example.carrental.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.carrental.dto.RentalStatus;
import com.example.carrental.dto.ResponseDto;
import com.example.carrental.service.RentalService;

@SpringBootTest
class RentControllerTest {

	
	@InjectMocks
	RentalController rentalController;
	
	@Mock
	RentalService rentalService;
	
	@Test
	void testUpdateStatus() {
		ResponseDto responseDto = new ResponseDto();
		responseDto.setReason("Processed Successfully");
		responseDto.setStatusCode("2001");
		responseDto.setStatusMessage("Rental request processed");
		
		when(rentalService.UpdateStatus(any(), any(), any(), any())).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = rentalController.UpdateStatus(1l, RentalStatus.ACCEPT,1l, "Accepted");
		
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
}





