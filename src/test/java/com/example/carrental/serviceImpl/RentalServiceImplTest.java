package com.example.carrental.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.example.carrental.dto.ApiResponse;
import com.example.carrental.dto.RentalResponse;
import com.example.carrental.dto.RentalStatus;
import com.example.carrental.dto.Role;
import com.example.carrental.dto.ViewRentalResponse;
import com.example.carrental.entity.Rental;
import com.example.carrental.entity.User;
import com.example.carrental.entity.Vehicle;
import com.example.carrental.exception.InvalidRoleException;
import com.example.carrental.exception.RentalDetailsException;
import com.example.carrental.exception.ResourceNotFound;
import com.example.carrental.repository.RentalRepository;
import com.example.carrental.repository.UserRepository;
import com.example.carrental.utils.Response;

@SpringBootTest
public class RentalServiceImplTest {
	@Mock
	private RentalRepository rentalRepository;
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private RentalServiceImpl rentalServiceImpl;

	ApiResponse apiResponse = new ApiResponse(Response.SUCCESS_STATUS_CODE, Response.SUCCESS_STATUS_MESSAGE);
	RentalResponse RentalResponse = new RentalResponse(1l, 5l, LocalDate.now(), LocalDate.now(), RentalStatus.ACCEPT,
			"Marriage");
	List<RentalResponse> responseList = new ArrayList<>();
	List<Rental> rentalList = new ArrayList<>();
	Vehicle veh = new Vehicle();
	User usr = new User(1l, "anu", "64523379", Role.AGENT);
	Rental rental = new Rental(1l, veh, usr, LocalDate.now(), LocalDate.now(), new BigDecimal("200.0"),
			RentalStatus.ACCEPT, "Marriage");

	@Test
	void testInvaliduserDetailsExceptionThrown() {
		User usr1 = new User(1l, "anu", "64523379", Role.CUSTOMER);
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usr1));
		assertThrows(InvalidRoleException.class, () -> rentalServiceImpl.viewRentals(1l, 0, 2));

	}

	@Test
	void testviewrentalsrentalDetailsExceptionThrown() {

		Pageable pageable = PageRequest.of(0, 2);
		Page<Rental> emptyTransactionPage = new PageImpl<>(List.of());
		when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usr));

		when(rentalRepository.findAll(pageable)).thenReturn(emptyTransactionPage);

		RentalDetailsException exception = assertThrows(RentalDetailsException.class,
				() -> rentalServiceImpl.viewRentals(1l, 0, 2));
		assertEquals(Response.NO_RECORD_FOUND_CODE, exception.getCode());

	}



	@Test
	void testviewrentalsSuccess() {
		responseList.add(RentalResponse);
		Page<RentalResponse> pageResponse = new PageImpl<>(responseList);
		ViewRentalResponse response = new ViewRentalResponse(apiResponse, pageResponse);
		rentalList.add(rental);

		Pageable pageable = PageRequest.of(0, 2);
		Page<Rental> emptyTransactionPage = new PageImpl<>(rentalList);

		when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usr));
		when(rentalRepository.findAll(pageable)).thenReturn(emptyTransactionPage);
		ViewRentalResponse apiResponse = rentalServiceImpl.viewRentals(1l, 0, 2);
		assertEquals(response.getApiResponse(), apiResponse.getApiResponse());

	}
	
	@Test
	void testUserNotFound() {
		when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		ResourceNotFound exception=assertThrows(ResourceNotFound.class,
				() -> rentalServiceImpl.viewRentals(1l, 0, 2));
		assertEquals(Response.USER_NOT_FOUND_CODE, exception.getCode());

	}

}
